package ku.util;

import java.util.NoSuchElementException;
/**
 * This is ArrayIterator that can do task like Array and also be able to check 
 * 		the size and the data within itself.
 * @author Thanawit Gerdprasert.
 * @param <T> type of the data type.
 */
public class ArrayIterator<T> implements java.util.Iterator<T> {

	private T[] array;
	private int calledNumber;


	/**
	 * Construct the ArrayIterator and initialize the number and the array that recieved from parameter.
	 * @param recieve array that will be initialize.
	 */
	public ArrayIterator (T[] recieve)
	{
		array = recieve; 
		calledNumber=0;
	}

	/**
	 * Called this class to check whether or not the Iterator has next element or not.
	 * @return true if Iterator has the next element, else return false.
	 */
	public boolean hasNext() 
	{
		for(int i =calledNumber ; i<array.length;i++)
		{
			if(array[i]!=null)
				return true;

		}
		return false;
	}

	/**
	 * Check whether the next Object is available or not and then return it.
	 * @return the Object that come after this one and update the calledNumber.
	 */
	public T next() 
	{
		if(this.hasNext())
		{
			if(array[calledNumber]==null)
			{
				for(  ; calledNumber < array.length;calledNumber++)
				{
					if(array[calledNumber]!=null)
					{
						int temp = calledNumber;
						calledNumber++;
						return array[temp];
					}
				}
			}
			else
			{
				int temp = calledNumber;
				calledNumber++;
				return array[temp];
			}
		}
		else
		{
			throw new NoSuchElementException();
		}
		return null;
	}

	/**
	 * Remove the Object that recieved from next() method.
	 */
	public void remove()
	{	
			array[calledNumber-1]=null;
	}
}
