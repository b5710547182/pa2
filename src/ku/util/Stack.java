package ku.util;

import java.util.EmptyStackException;
/**
 * This is the Stack class that look like the Array of Object with some 
 * @author Thanawit Gerdprasert
 *
 * @param <T>
 */
public class Stack<T>{

	private T[] items;
	private int stackCapacity;

	/**
	 * Create Object called Stack with items inside it and its own capacity;
	 * @param capacity
	 */
	public Stack (int capacity)
	{
		if(capacity<0)
		{
			items = (T[]) new Object[0];
			stackCapacity =0 ;
		}
		else
		{
			items = (T[]) new Object[capacity] ;
			stackCapacity = capacity;
		}
	}
	/**
	 * Return the capacity of the Stack.
	 * @return the capacity of the stack.
	 */
	public int capacity()
	{
		if(stackCapacity > 0 )
			return this.stackCapacity;
		else
			return -1;	
	}
	/**
	 * Check whether the stack is empty or not.
	 * @return true if Stack is empty, else return false.
	 */
	public boolean isEmpty()
	{	
		int checker =items.length;
		for(int i=0 ; i < items.length;i++)
		{
			if(items[i]==null)
			{
				checker--;
			}
		}
		if(checker==0)
			return true;
		else 
			return false;
	}
	/**
	 * Check whether the stack is full or not.
	 * @return true if the Stack is full else return false.
	 */
	public boolean isFull()
	{

		if(this.size()==items.length)
			return true;
		else 
			return false;

	}
	/**
	 * Use this to check the top of element and return it without removing it.
	 * @return the element at the top if the stack is not empty.
	 */
	public T peek()
	{
		if(this.isEmpty())
			return null;
		else
		{
			int i = items.length-1;
			while(i>=0)
			{
				if(items[i]!=null)
				{
					break;
				}
				else
					i--;
			}
			return items[i];
		}
	}
	/**
	 * Remove element at the Top of the Stack.
	 * @return the top of element of stack if it's not null.
	 */
	public T pop()
	{
		if(this.isEmpty())
		{
			throw new EmptyStackException() ;
		}
		else
		{
			T tempObject = (T) new Object() ;
			if(this.isEmpty())
				return null;
			else
			{
				int i = items.length-1;
				while(i>=0)
				{
					if(items[i]!=null)
					{
						tempObject = items[i];
						break;
					}
					else
						i--;
				}
				items[i]=null;
				return tempObject;
			}
		}
	}
	/**
	 * add the Object T to the top of Stack.
	 * @param obj that will be added to the stack.
	 */
	public void push(T obj)
	{
		if(this.isFull())
		{
			throw new IllegalArgumentException();
		}
		else
		{
			int emptySpot = 0;
			for(int i =0 ; i<items.length;i++)
			{
				if(items[i]==null)
				{
					emptySpot = i;
					break;
				}
			}
			items[emptySpot] = obj;

		}
	}
	/**
	 * Return size of currect Stack.
	 * @return integer is size of the stack.
	 */
	public int size()
	{
		int returnNumber =0;
		for(int i =0 ; i < items.length;i++)
		{
			if(items[i]==null)
			{
				break;
			}
			returnNumber++;
		}
		return returnNumber;
	}


}
